<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@client');
Route::get('/tolk', 'PageController@tolk');
Route::get('/settings', 'PageController@settings');
Route::get('/ac/active', 'ACController@getCompiledAc');
Route::get('/ac', 'AcController@getAcJSON');


Route::get('/settings/autocorrect', function() {
    return "";
});
Route::post('/settings/autocorrect', function () {
    return "";
});
Route::post('/settings/update', 'SettingsController@update');

Route::get('/test', 'ACController@getAcJSON');