<?php

use Illuminate\Database\Seeder;
use App\AcList;
use App\Autocorrect;

class ac_list_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lijst = new AcList();
        $lijst->name = "jantje";
        $lijst->save();

        $woord = new AutoCorrect();
        $woord->key = "abc";
        $woord->value = "def";

        $lijst->autocorrects()->save($woord);
    }
}
