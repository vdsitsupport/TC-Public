<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcList extends Model
{
    public function autocorrects() {
        return $this->hasMany('App\AutoCorrect');
    }
}
