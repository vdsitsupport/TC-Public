<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    public function update(Request $request) {
        $input = $request->all();
        //var_dump($input);
        foreach ($input as $k => $v) {
            if ($k == '_token') {
                continue;
            }
            else {
                echo "$k: $v";
            }
            $s = Setting::firstOrCreate(['key' => $k]);
            $s->value = $v;
            $s->save();
        }

        return back();
    }

    public static function settingsArray() {
        $settings = Setting::all();
        $result = [];
        foreach ($settings as $s) {
            $result[$s->key] = $s->value;
        }
        return $result;
    }
}
