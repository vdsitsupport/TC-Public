<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function tolk() {
        return view('tolk', [
            'settings' => SettingsController::settingsArray(),
            'ac' => ACController::getCompiledAc(),
        ]);
    }

    public function client() {
        return view('client', [
            'settings' => SettingsController::settingsArray(),
        ]);
    }

    public function settings() {
        return view('settings', ['settings' => SettingsController::settingsArray()]);
    }
}
