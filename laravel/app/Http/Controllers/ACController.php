<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AcList;
use App\AutoCorrect;

class ACController extends Controller
{
    public static function getCompiledAc() {
        $result = [];

        $lists = AcList::all();
        //var_dump($lists);
        foreach ($lists as $list) {
            if ($list->enabled) {
                //$row = [];
                $words = $list->autocorrects;
                foreach ($words as $w) {
                    $result[$w->key] = $w->value;
                    //echo $w->key . " - " . $w->value;
                }
                //$result[$list->name] = $row;
            }
        }
        return json_encode($result);
    }

    public static function getAcJSON() {
        $result = [];

        $lists = AcList::all();
        //var_dump($lists);
        foreach ($lists as $list) {
            $row = [];
            $words = $list->autocorrects;
            foreach ($words as $w) {
                $row[$w->key] = $w->value;
                //echo $w->key . " - " . $w->value;
            }
            $result[$list->name] = $row;
        }
        return json_encode($result);
    }
}
