<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoCorrect extends Model
{
    public function aclist() {
        return $this->belongsTo('App\AcList');
    }
}
