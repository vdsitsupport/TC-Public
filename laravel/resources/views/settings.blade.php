
<html>
<head>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/webfont.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="/js/bootstrap.min.js"  crossorigin="anonymous"></script>


    <script src="js/cookie.js"></script>
    <script src="js/spectrum.js"></script>
    <link rel="stylesheet" href="css/spectrum.css">
    <script src="js/socket.io.js"></script>
    <style>


        body {
            font-family:Arial, Helvetica, sans-serif;
        }
    </style>

    <script>
        $(document).ready(function() {
            $("#button_verbindingen").on("click", function() {
                hideMiddle();
                $("#div_verbindingen").show();
            });

            $("#button_autocorrect").on("click", function() {
                hideMiddle();
                $("#div_autocorrect").show();
            });

            $("#btn_socketio").on("click", function() {
                hideRight();
                $("#div_socketio").show();
            });

            $("#form_socketio").submit(function(e) {
                // PreventDefault als er een fout is.
                //e.preventDefault();


            })
        })

        function hideMiddle() {
            hideRight();
            $("#div_middle > div").each(function(index) {
                $(this).hide();
            });
        }

        function hideRight() {
            $("#div_right > div").each(function(index) {
                $(this).hide();
            });
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="center-block text-center">
                <h1>
                    Instellingen.
                </h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="btn-group-vertical">
                <button type="button" id="button_autocorrect" class="btn btn-default">
                    Autocorrect
                </button>
                <button type="button" id="button_verbindingen" class="btn btn-danger">
                    Netwerkinstellingen
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
                <button type="button" class="btn btn-default">
                    Instelling 2
                </button>
            </div>

        </div>
        <div class="col-md-4" id="div_middle">
            <div class="collapse" id="div_autocorrect">
                <div class="btn-group-vertical">
                    <span>
                        <input type="checkbox">
                        <button type="button" class="btn btn-default">
                            Jantje
                        </button>
                    </span>
                    <button type="button" class="btn btn-default">
                        Pietje
                    </button>
                    <button type="button" class="btn btn-default">
                        Kerk
                    </button>
                    <button type="button" class="btn btn-default">
                        School
                    </button>
                </div>
            </div>

            <div class="collapse" id="div_verbindingen">
                <button type="button" id="btn_socketio" class="btn btn-danger">
                    Socket.io
                </button>
            </div>
        </div>
        <div class="col-md-4" id="div_right">
            <div class="collapse" id="div_socketio">
                <form id="form_socketio" method="post" action="settings/update">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="io_ip">Socket.io IP</label>
                        <input type="text" class="form-control" name="io_ip" id="io_ip" value="{{$settings['io_ip'] or ""}}" placeholder="Socket.io IP">
                    </div>
                    <div class="form-group">
                        <label for="io_port">Socket.io Port</label>
                        <input type="text" class="form-control" name="io_port" id="io_port" placeholder="Socket.io Port" value="{{$settings['io_port'] or ""}}">
                    </div>
                    <div class="form-group">
                        <input type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>