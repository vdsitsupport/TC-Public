
<html>
<head>
    <script src="/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/webfont.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="/js/bootstrap.min.js"  crossorigin="anonymous"></script>


    <script src="js/cookie.js"></script>
    <script src="js/spectrum.js"></script>
    <link rel="stylesheet" href="css/spectrum.css">
    <script src="js/socket.io.js"></script>
    <style>
        #text {
            margin: 25px;
            padding-top: 100px;
            word-wrap: break-word;
        }

        body {
            font-family:Arial, Helvetica, sans-serif;
        }

        #topbody {
            background-color: #2e2e2e;
            padding: 25px;
            position: fixed;
            top: 0;
        }

        .size50 {
            width: 50px;
            height: 50px;
        }
    </style>

    <script>

        var fontsize;
        var bgcolor;
        var fontcolor;


        $(document).ready(function() {

            fontsize = Cookies.get('fontsize') === undefined ? 20 : Cookies.get('fontsize');
            bgcolor = Cookies.get('bgcolor') === undefined ? "000000" : Cookies.get('bgcolor');
            fontcolor = Cookies.get('fontcolor') === undefined ? "FFFFFF" : Cookies.get('fontcolor');

            $("#text").css({
                fontSize: fontsize,
            });

            $("#instellingen").on('click', function() {
                $("#clickdiv").toggle();
            })
            $("#clickdiv").toggle();

            $("#fontincrease").on('click', function() {
                fontsize = fontsize * 1.10;
                Cookies.set('fontsize', fontsize);
                $("#text").css({
                    fontSize: fontsize,
                })
                console.log('increase font size');
            });

            $("#fontdecrease").on('click', function() {
                fontsize = fontsize / 1.10;
                Cookies.set('fontsize', fontsize);
                $("#text").css({
                    fontSize: fontsize,
                })
                console.log('decrease font size');
            });


            var ip = "{{$settings['io_ip'] or '192.168.0.1'}}";
            var port = "{{$settings['io_port'] or '8080'}}";
            socket = io(ip + ':' + port);

            socket.on('tolk', function(msg){
                console.log('msg received on tolk');
                $('#text').html(msg);
                $(document).scrollTop( $("#bottom").offset().top );
                socket.emit('resp', msg);
            });

            $("#textcolor").spectrum({
                change: function(color) {
                    console.log('new text color' + color.toHexString()); // #ff0000
                    fontcolor = color.toHexString();
                    Cookies.set('fontcolor', fontcolor);
                    updateColors();
                },
                color: fontcolor,
            });
            $("#bgcolor").spectrum({
                change: function(color) {
                    console.log('new bg color: '+color.toHexString());
                    bgcolor = color.toHexString();
                    Cookies.set('bgcolor', bgcolor);
                    updateColors();
                },
                color: bgcolor,
            });
            updateColors();
        });

        function updateColors() {
            $("#text").css('color', fontcolor);
            $("body").css('background-color', bgcolor);
        }
    </script>
</head>
<body>
<div id="topbody">
    <div id="buttondiv">
        <button id="instellingen" class="size50">&#9776;</button>
                <span id="clickdiv">
                    <span id="instellingendiv">

                    </span>
                    <span>
                        <button id="fontincrease" class="size50">F+</button>
                    </span>
                    <span>
                        <button id="fontdecrease" class="size50">F-</button>
                    </span>
                    <span>
                        Text: <input type="color" id="textcolor"/>
                    </span>
                    <span>
                        Achtergrond: <input type="color" id="bgcolor"/>
                    </span>
                    <span class="ui-icon ui-icon-locked size50"></span>
                </span>
    </div>
</div>
<div id="text">&nbsp;</div>
<a name="bottom" id="bottom"></a>
</body>
</html>