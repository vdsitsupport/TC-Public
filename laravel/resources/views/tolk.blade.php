
<html>
<head>
    <script src="/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/jquery-ui.theme.css">
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/webfont.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/spectrum.css">
    <script src="js/spectrum.js"></script>
    <link rel="stylesheet" href="css/jquery-te-1.4.0.css">
    <script src="js/jquery-te-1.4.0.js"></script>
    <script src="js/socket.io.js"></script>

    <style>
        #tolktekst {
            width: 100%;
            height: 100%;
        }
        #showsettings {
            position: absolute;
            right: 10px;
        }
        #topbody {
            background-color: #2e2e2e;
            color: white;
        }
        .jqte {
            width: 100%;
            height: calc(100% - 200px);
        }

    </style>

    <script>
        var acactief = {!! $ac !!};


        (function( $ ){
            $.fn.findNextOverall = function(sel, returnItselfIfMatched) {

                if(returnItselfIfMatched && $(this).is(sel)) return $(this);

                var $result = $(sel).first();
                if ($result.length <= 0) {
                    return $result;
                }
                $result = [];
                var thisIndex = $('*').index($(this));
                var selIndex = Number.MAX_SAFE_INTEGER;
                $(sel).each(function(i,val){
                    var valIndex = $('*').index($(val));
                    if (thisIndex < valIndex && valIndex < selIndex) {
                        selIndex = valIndex;
                        $result = $(val);
                    }
                });
                return $result;
            };
        })( jQuery );

        var currentlist = null;
        $(document).ready(function() {
        width = $(window).width();
        height = $(window).height();

        bgcolor = Cookies.get('bgcolor') === undefined ? "000000" : Cookies.get('bgcolor');
        fontcolor = Cookies.get('fontcolor') === undefined ? "FFFFFF" : Cookies.get('fontcolor');

        var ip = "{{$settings['io_ip'] or '192.168.0.1'}}";
        var port = "{{$settings['io_port'] or '8080'}}";
        socket = io(ip + ':' + port);

        socket.on('resend', function(msg){
            socket.emit('tolk', $("#tolktekst"));
        });

        socket.on('resp', function(resp) {
            if (resp.length > 50) {
                resp = resp.substr(resp.length - 45);
            }
            $("#clientresponse").html(resp);
        })

        if (localStorage.getItem('autocorrect') !== null) {
            autocorrect = JSON.parse(localStorage.getItem('autocorrect'));
        }

        if (localStorage.getItem('tolktekst') !== null) {
            $("#tolktekst").html(localStorage.getItem('tolktekst'));
        }
        //$("#tolktekst").autocorrect({ corrections: { aboutit: "about it" } });
        $("#tolktekst").bind('input propertychange', function(event) {
            // Autocorrect
            resend = false;
            var content = $("#tolktekst").val();
            var lastword = content.substr(content.lastIndexOf(" ")+1);
            var before = content.substr(0, content.lastIndexOf(" ")+1);

            for (var key in acactief) {
                if (lastword.lastIndexOf(key)!==-1) {
                    var lastword = lastword.replace(new RegExp(key), acactief[key]);

                    $("#tolktekst").val(before+lastword);
                    resend = true;
                }
            }
            if (!resend) {
                var last = content.substr(content.length - 1);
                if (last == " ") { resend = true; }
            }
            if (resend) {
                SendCurText();
            }
        });
        SendCurText();

        //$("#settingsbar").toggle();
        $("#showsettings").on('click', function() {
            $("#settingsbar").toggle();
        });


        $("#textcolor").spectrum({
            change: function(color) {
                console.log('new text color' + color.toHexString()); // #ff0000
                fontcolor = color.toHexString();
                Cookies.set('fontcolor', fontcolor);
                updateColors();
            },
            color: fontcolor,
        });
        $("#bgcolor").spectrum({
            change: function(color) {
                console.log('new bg color: '+color.toHexString());
                bgcolor = color.toHexString();
                Cookies.set('bgcolor', bgcolor);
                updateColors();
            },
            color: bgcolor,
        });
        updateColors();

        $("#version").html("V0.0.2");

        });

        function SendCurText() {
            var txt = $("#tolktekst").val();
            //$.post( "text/post", { text: txt } );
            socket.emit('tolk', txt);
            localStorage.setItem('tolktekst', txt);
            console.log('updated');
            //setTimeout(SendCurText, 2000);
        }

        function updateColors() {
            $("#tolktekst").css('color', fontcolor);
            $("#tolktekst").css('background-color', bgcolor);
        }


    </script>


</head>
<body>
<div id="topbody">
    <div id="topbar">
        <span id="version"></span>
        - <a href="http://192.168.0.1/client">http://192.168.0.1/client</a>
        <button id="showsettings">Instellingen</button>
    </div>
    <div id="settingsbar">
        <button id="showautocorrect">Auto correctie lijst</button>
            <span>
                        Text: <input type="color" id="textcolor"/>
                    </span>
            <span>
                        Achtergrond: <input type="color" id="bgcolor"/>
                    </span>
        <div id="clientresponse"></div>

    </div>
    <div id="autocorrect">
        <table id="autocorrecttable" border="1">

        </table>
        <div>
            <a href="#" id="newlinecorrect">
                <span class="ui-icon ui-icon-plus"></span>
            </a>
        </div>
    </div>
    <div id="div_showAC">
        <table id="table_showAC"></table>
        <div>
            <a href="#" id="a_ACnew">
                <span class="ui-icon ui-icon-plus"></span>
            </a>
        </div>
    </div>
</div>
<textarea id="tolktekst">Welkom. </textarea>
</body>
</html>